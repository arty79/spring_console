package com.noname.test;

import com.noname.pack.*;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 * Created by arty on 04.10.2016.
 */
public class MainTest {

    @InjectMocks
    ConsoleHelper consoleHelper;

    PlusRequest plusRequest;

    MinusRequest minusRequest;

    InfoRequest infoRequest;


    @BeforeMethod
    public void setUp() {
        ClassPathXmlApplicationContext ac = new ClassPathXmlApplicationContext(new String[]{"config.xml"});
        consoleHelper = ac.getBean(ConsoleHelper.class);
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void test1() {
        String plus = "plus";
        plusRequest = (PlusRequest) consoleHelper.execute(plus);
        plusRequest.operate();
        Counter counter = plusRequest.getCounter();
        Assert.assertEquals(counter.getCount(), 1);
    }
    @Test
    public void test2() {
        String plus = "minus";
        minusRequest = (MinusRequest) consoleHelper.execute(plus);
        minusRequest.operate();
        Counter counter = minusRequest.getCounter();
        Assert.assertEquals(counter.getCount(), -1);
    }
    @Test
    public void test3() {
        String plus = "info";
        infoRequest = (InfoRequest) consoleHelper.execute(plus);
        infoRequest.operate();
        Counter counter = infoRequest.getCounter();
        Assert.assertEquals(counter.getCount(), 0);
    }
}