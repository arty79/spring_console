package com.noname.pack;

import lombok.Getter;
import lombok.Setter;

/**
 * ����� <class>MinusRequest</class>
 *
 * @author user
 */
public class MinusRequest implements Request {

    @Getter
    @Setter
    private Counter counter;

    public void operate(){
        int count = counter.getCount();
        counter.setCount(--count);
    }
}
