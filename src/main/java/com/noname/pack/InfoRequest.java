package com.noname.pack;

import lombok.Getter;
import lombok.Setter;

/**
 * ����� <class>InfoRequest</class>
 *
 * @author user
 */
public class InfoRequest implements Request {

    @Getter
    @Setter
    private Counter counter;

    public void operate(){
        System.out.println(counter.getCount());
        }
}
