package com.noname.pack;

import lombok.Getter;
import lombok.Setter;

/**
 * ����� <class>PlusRequest</class>
 *
 * @author user
 */
public class PlusRequest implements Request {

    @Getter
    @Setter
    private Counter counter;

    @Override
    public void operate() {
        int count = counter.getCount();
        counter.setCount(++count);
    }
}
